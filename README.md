# Introducción

**GeoIp** es un programa destinado a la geolocalización de una dirección IP con licencia libre para su uso sin restricción alguna.
No solo permite la obtención de la Geolicalización, sino que también permite obtener el **Sistema Autónomo** (ASN) al que pertenece la IP y la organización que lo tiene registrado.

Se encuentra escrita en **Python3**, adjuntando también un script escrito en **Bash** para actualizar las bases de datos de **MaxMind** periódicamente desde el **Cron** del sistema.

Para ello se utilizan las siguientes herramientas:

## MaxMind

Bases de datos de [MaxMind](https://www.maxmind.com/en/home), en concreto las bases de datos City y ASN de su producto gratuito [GeoLite2](https://dev.maxmind.com/geoip/geoip2/geolite2/).

Tras los últimos cambios aplicados el **30 de Diciembre del 2019** es necesario disponer de una cuenta gratuita en esta plataforma, pudiendo registrarse en el siguiente [enlace](https://www.maxmind.com/en/geolite2/signup).

Una vez creada la cuenta hay que generar un código o ***token*** para poder descargarse las bases de datos. Este código lo generamos, una vez validados en el portal, en el siguiente [enlace](https://www.maxmind.com/en/accounts/current/license-key).

Las instrucciones para descargar los archivos a partir de su URL se pueden consultar [aquí](https://dev.maxmind.com/geoip/geoipupdate/#Direct_Downloads).

Toda la información de estos cambios se pueden consultar en el siguiente [enlace](https://blog.maxmind.com/2019/12/18/significant-changes-to-accessing-and-using-geolite2-databases/).

## Nominatim

A partir de las coordenadas de *latitud* y *longitud* obtenidas con las bases de datos de **MaxMind** obtenemos la ubicación con la herramienta [Nominatim](https://wiki.openstreetmap.org/wiki/Nominatim), la cual hace uso de la información de **OpenStreetMap**.

Esta herramienta tiene una serie de restricciones y limitaciones que debemos de tener en cuenta para su uso. Se encuentran [aquí](https://operations.osmfoundation.org/policies/nominatim/).


# Instalación

Se recomienda hacer uso de *entornos virtuales* de **Python** para desplegar la herramienta. Puedes consultar como instalarlo en entornos Linux [aquí](https://www.tiraquelibras.com/blog/?p=723).

## Descarga del programa

Nos ubicamos en el directorio en donde vayamos a desplegar el programa.

```bash
cd /path/to/your/program
```

Descartamos el programa con el siguiente comando:

```bash
git clone https://sergiobr@bitbucket.org/sergiobr/geoip.git
```

## Instalación de módulos necesarios

Instalamos los módulos necesarios indicados en el archivo ***requirements.txt*** con el siguiente comando:

```bash
pip install -r requirements.txt
```

## Descarga/Actualización de las bases de datos de MaxMind

Primero editamos el archivo **updateMaxMind.sh** e indicamos nuestro ***token*** en la siguiente línea:

```
# Max Mind token
maxMind_token='YOUR_MAXMIND_TOKEN'
```

Sustituimos ***YOUR_MAXMIND_TOKEN*** por nuestro ***token*** creado.

Ejecutamos el script de **Bash** para descargar las bases de datos de **MaxMind** que vamos a utilizar:

```bash
./updateMaxMind.sh
```

Podemos configurar este en el **Cron** de nuestro sistema, por ejemplo para que se ejecute los *lunes* y *viernes*:

```
02 19 * * 1,5 cd /path/to/your/program; ./updateMaxMind.sh
```

# Ejecución del programa

Importante ejecutar el programa en un entorno con **Python3** instalado.

El programa dispone de una ayuda contextual descriptiva:

```bash
# python geoIp.py --help
Usage: Address info of an IP address.
Usage: geoIp.py [options] ip_address.

IP geolocation info. Add only one IP address. Ranges are not allowed.

Options:
  --version   show program's version number and exit
  -h, --help  show this help message and exit
  -a, --asn   Get only ASN info.
  -f, --full  Get full address info.

Powered by https://www.tiraquelibras.com/blog
```

Las opciones disponibles son:

- -h o --help: permite mostrar la ayuda del programa.
- -a o --asn: muestra la información del ASN junto a la información por defecto.
- -f o --full: muestra la información por defecto ampliada.
- --version: muestra la versión desplegada del programa.

Un ejemplo de ejecución con informaicón por defecto:

```
# python geoIp.py 8.8.8.8
IP               -  8.8.8.8
Latitude         -  37.751
Longitude        -  -97.822
ContCode         -  NA
Continent        -  North America
CounCode         -  US
Country          -  United States

Powered by https://www.tiraquelibras.com/blog
```

Otro ejemplo con información del ***ASN***:

```
# python geoIp.py 8.8.8.8 -a
IP               -  8.8.8.8
Latitude         -  37.751
Longitude        -  -97.822
ContCode         -  NA
Continent        -  North America
CounCode         -  US
Country          -  United States
ASN              -  15169
ASN Org          -  Google LLC

Powered by https://www.tiraquelibras.com/blog
```

Y por último con información ampliada:

```
# python geoIp.py 8.8.8.8 -f
IP               -  8.8.8.8
Latitude         -  37.751
Longitude        -  -97.822
ContCode         -  NA
Continent        -  North America
CounCode         -  US
Country          -  United States
Region           -  Kansas
Address          -  Reno County, Kansas, United States
Timezone         -  America/Chicago
Licence          -  Data © OpenStreetMap contributors, ODbL 1.0. https://osm.org/copyright

Powered by https://www.tiraquelibras.com/blog
```

Versión del programa:

```
# python geoIp.py --version
geoIp.py: version 1.0

Powered by https://www.tiraquelibras.com/blog
```


