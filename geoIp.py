import maxminddb
import sys
from geopy.geocoders import Nominatim
from optparse import OptionParser
from IPy import IP

######################
# Funciones y Clases #
######################

# Clase para interactuar con MaxMind y Nominatim
class getIpInfo: 
    
    # Constructor de la clase
    def __init__(self,ip):
        self.ip = str(ip)

    # Obtener la info de MaxMind
    def getMaxMind(self):
        try:
            # Obtener la info de la base de datos City de MaxMind
            reader = maxminddb.open_database('dbs/GeoLite2-City.mmdb')
            res = reader.get(self.ip)
            # Devolver el resultado de MaxMind
            return res
        except Exception as e:
            raise ValueError('class - error getting MaxMind info: %s' % (str(e)))

    # Funcion para obtener la direccion a partir de las coordenadas Latitud y Longitud
    def getAddress(self,coordenates):      
        try:
            # Crear la aplicacion de Nominatim
            geolocator = Nominatim(user_agent="my-application")            
            # Obtener la localizacion a partir de la Latitud y Longitud
            location = geolocator.reverse(coordenates)
            # Devolver la informacion con la localizacion
            return location.raw
        except Exception as e:
            raise ValueError('class - error getting Nominatim info from coordenates: %s' % (str(e)))
        

    # Obtener la info del ASN de MaxMind
    def getAsnInfo(self):
        try:
            # Obtener la info de la base de datos ASN de MaxMind
            reader = maxminddb.open_database('dbs/GeoLite2-ASN.mmdb')
            res = reader.get(self.ip)
            # Devolver el resutlado de MaxMind
            return res
        except Exception as e:
            raise ValueError('class - getting MaxMind ASN info: %s' % (str(e)))


# Mostar el resultado final
def printResult(info,arguments):
    try:
        if info == 'default':
            keys = ['IP','Latitude','Longitude','ContCode','Continent','CounCode','Country']
            for key in keys:
                print('%-15s  -  %-15s' % (str(key), str(arguments[key])))
        elif info == 'full':
            keys = ['Region','Address','Timezone','Licence']
            for key in keys:
                print('%-15s  -  %-15s' % (str(key), str(arguments[key])))
        elif info == 'asn':
            keys = ['ASN','ASN Org']
            for key in keys:
                print('%-15s  -  %-15s' % (str(key), str(arguments[key])))
        else:
            print('Nothing to show.')
    except Exception as e:
        raise ValueError('function - error printing results: %s' % (str(e)))

# Creacion de las opciones e info del programa
def programParser():
    try:
        usage = "Address info of an IP address.\nUsage: %prog [options] ip_address."
        description = 'IP geolocation info. Add only one IP address. Ranges are not allowed.'
        version = '%prog: version 1.1'
        parser = OptionParser(usage,version=version,description=description,add_help_option=True)
        parser.add_option('-a', '--asn', dest = 'asn', help = 'Get only ASN info.', action='store_true')
        parser.add_option('-f', '--full', dest = 'full', help = 'Get full address info.', action='store_true')
        return parser
    except Exception as e:
        raise ValueError('function - creating program parser: %s' % (str(e)))


############
# Programa #
############

try:
    # Crear el Parser del programa
    parser = programParser()

    # Obtener los argumentos y opciones
    (options, args) = parser.parse_args()

    # Confirmar el total de argumentos. Solo se permite uno
    if len(args) == 0:
        raise ValueError('No argument has been indicated.')
    elif len(args) > 1 or len(args) < 0:
        raise ValueError('Only one argument is allowed.')
    else:

        # Confirmar si el argumento es una IP valida o no
        try:
            ip = IP(args[0])
        except Exception as e:
            raise ValueError('The IP %s is not valid. %s' % (str(args[0]),str(e)))
        
        # Confirmar si la IP es publica o no
        if ip.iptype() == 'PUBLIC':

            # Crear la clase para obtener la informacion de la IP
            geoIpCommands = getIpInfo(ip)

            # Declaramos las variables con los resultados
            finalResult = {}
            finalResultFull = {}
            finalResultAsn = {}
        
            # Obtener la direccion de la IP dependiendo de los argumentos indicados
            # Obtener la info de MaxMind
            maxMindRes = geoIpCommands.getMaxMind()

            # Info por defecto
            finalResult = {'IP':str(ip)}
            finalResult['Latitude'] = str(maxMindRes['location']['latitude'])
            finalResult['Longitude'] = str(maxMindRes['location']['longitude'])
            finalResult['ContCode'] = str(maxMindRes['continent']['code'])
            finalResult['Continent'] = str(maxMindRes['continent']['names']['en'])
            if 'country' in maxMindRes:
                finalResult['CounCode'] = str(maxMindRes['country']['iso_code'])
                finalResult['Country'] = str(maxMindRes['country']['names']['en'])
            elif 'registered_country' in maxMindRes:
                finalResult['CounCode'] = str(maxMindRes['registered_country']['iso_code'])
                finalResult['Country'] = str(maxMindRes['registered_country']['names']['en'])
            else:
                finalResult['CounCode'] = 'None'
                finalResult['Country'] = 'None'

            # Info completa, opcion '-f' o '--full'
            if options.full:
                # Obtener las coordenadas (Latitud y Longitud)
                coordenates = finalResult['Latitude'],finalResult['Longitude']                
                # Obtener la info de Nominatim a partir de las cordenadas
                nominatimRes = geoIpCommands.getAddress(coordenates)
                # Info completa
                if 'state' in nominatimRes['address']:
                    finalResultFull['Region'] = nominatimRes['address']['state']
                elif 'state_district'           in nominatimRes['address']:
                    finalResultFull['Region'] = nominatimRes['address']['state_district']
                else:
                    finalResultFull['Region'] = 'None'
                finalResultFull['Address'] = nominatimRes['display_name']
                finalResultFull['Timezone'] = str(maxMindRes['location']['time_zone'])           
                finalResultFull['Licence'] = nominatimRes['licence']
                #finalResultFull['City'] = str(maxMindRes['city']['names']['en'])
                #finalResultFull['Postcode'] = str(maxMindRes['postal']['code']                
                #finalResultFull['Postcode'] = nominatimRes['address']['postcode']

            # Info del ASN, opcion '-a' o '--asn'
            if options.asn:
                # Obtener la info del ASN
                asnRes = geoIpCommands.getAsnInfo()
                # Info del ASN
                finalResultAsn['ASN'] = asnRes['autonomous_system_number']
                finalResultAsn['ASN Org'] = asnRes['autonomous_system_organization']

            # Mostrar los resultados 'autonomous_system_number' y 'autonomous_system_organization'
            printResult('default',finalResult)
            if finalResultFull:
                printResult('full',finalResultFull)
            if finalResultAsn:
                printResult('asn',finalResultAsn)
        
        else:
            raise ValueError('The IP %s is not public.' % (str(ip)))

except Exception as e:
    print('ERROR - Reason: %s' % (str(e)))
    sys.exit(1)
finally:
    print('\nPowered by https://www.tiraquelibras.com/blog\n')
