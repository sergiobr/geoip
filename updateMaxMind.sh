#!/bin/bash

### Variables

# DB files path
db_path='dbs'

# DB files name 
city_file='GeoLite2-City.mmdb'
country_file='GeoLite2-Country.mmdb'
as_file='GeoLite2-ASN.mmdb'

# DB files downloaded
city_file_gz='GeoLite2-City.tar.gz'
country_file_gz='GeoLite2-Country.tar.gz'
as_file_gz='GeoLite2-ASN.tar.gz'

# Max Mind token
maxMind_token='YOUR_MAXMIND_TOKEN'

# URLs to download all databases (City, Country, ASN)
city_url="https://download.maxmind.com/app/geoip_download?edition_id=GeoLite2-City&license_key=$maxMind_token&suffix=tar.gz"
country_url="https://download.maxmind.com/app/geoip_download?edition_id=GeoLite2-Country&license_key=$maxMind_token&suffix=tar.gz"
as_url="https://download.maxmind.com/app/geoip_download?edition_id=GeoLite2-ASN&license_key=$maxMind_token&suffix=tar.gz"


### Getting all files

# City
#echo 'uno city'
wget -c $city_url -O $city_file_gz
if [ $? != 0 ];then
        echo "Failed gettig $city_file_gz MaxMind database"
        exit 1
fi
#echo 'dos city'

# Country
#echo 'uno country'
wget -c $country_url -O $country_file_gz
if [ $? != 0 ];then
        echo "Failed gettig $country_file_gz MaxMind database"
        exit 1
fi
#echo 'dos country'

# ASN
#echo 'uno asn'
wget -c $as_url -O $as_file_gz
if [ $? != 0 ];then
	echo "Failed gettig $as_file_gz MaxMind database"
	exit 1
fi
#echo 'dos asn'

### Deleting all stored databases (.mmdb)
rm -rf $db_path/*

### Uncompress all files downloaded

# City
tar xzvf $city_file_gz 
# Country
tar xzvf $country_file_gz 
# ASN
tar xzvf $as_file_gz

### Moving all db files (.mmdb) to the final path

# City
path=$( find . -name $city_file -exec dirname {} \;)
mv $path'/'$city_file $db_path'/'

# Country
path2=$( find . -name $country_file -exec dirname {} \;)
mv $path2'/'$country_file $db_path'/'

# ASN
path3=$( find . -name $as_file -exec dirname {} \;)
mv $path3'/'$as_file $db_path'/'

rm -rf ./Geo*

exit 0
